import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMethodPage } from './edit-method.page';

describe('EditMethodPage', () => {
  let component: EditMethodPage;
  let fixture: ComponentFixture<EditMethodPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMethodPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMethodPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
